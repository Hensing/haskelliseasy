# Documentation

Sources of documentation

- [Hackage](http://hackage.haskell.org/) is the package repository for Haskell packages and also the server which can be run for private packages

- [Hoogle](https://www.stackage.org/lts-3.20/hoogle) allows searching for functions by their types or names - can also be installed locally for local and private packages

- [HLint](http://hackage.haskell.org/package/hlint) is a linting tool which allows providing [custom rules](https://github.com/ndmitchell/hlint#adding-hints)

- [pointfree](https://hackage.haskell.org/package/pointfree) can translate functions which take arguments to functions which just compose functions (e.g. `(\a -> a) = id)` - see also [Blunt](https://blunt.herokuapp.com/)

- [pointful](https://hackage.haskell.org/package/pointful) does the reverse of pointfree

- [Djinn](https://hackage.haskell.org/package/djinn) can derive some Haskell programs when just given a type signature

- [Argon](http://hackage.haskell.org/package/argon) gives cyclomatic complexity

- [stylish-haskell](https://hackage.haskell.org/package/stylish-haskell) is a code formatter with good defaults

- [packunused](http://hackage.haskell.org/package/packunused) lists unused packages from your .cabal file

- [git-vogue](https://hackage.haskell.org/package/git-vogue) is an aggregate code linter + git pre-commit hook which uses cabal, hlint, stylish-haskell and ghc-mod to ensure your code stays 'en vogue'
