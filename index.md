---
title: Home
---

# Haskell is easy - a list of recommended Haskell libraries

To make it more true that Haskell is easy, here's a list of curated libraries. In the personality descriptions matching you to a library, be honest with yourself about where you're at and what your needs are.

This isn't listing for listing's sake. I've either used it or someone whose taste and experience I really trust has used it. If you list everything, the list is useless to somebody that can't sniff test the API quickly.

## Disclaimer

I wrote Haskell book. I mention where things in this list (which is very opinionated) are covered in the book. I don't care if you buy our book.

## Getting Haskell installed

[Use Stack!](http://haskellstack.org)

## Basic data structures

- [containers](http://hackage.haskell.org/package/containers)

## Benchmarking

- [criterion](http://hackage.haskell.org/package/criterion) is a library for benchmarking. Criterion uses [statistics](#statistics) to help prevent you lying to yourself.
    - Covered along with basic data structures in [haskell book](http://haskellbook.com).

## Binary serialization/deserialization

#### Stuff that is somewhat opinionated about how your data looks over the wire

- [binary](http://hackage.haskell.org/package/binary)

- [cereal](http://hackage.haskell.org/package/cereal)

#### I wanna write my own!

- Use the [bytestring](http://hackage.haskell.org/package/bytestring) library and the builder API.

## Command-line argument parsing

- [optparse-applicative](https://hackage.haskell.org/package/optparse-applicative) is a command-line arguments parser library - easy flag/switch definitions and automatically creates useful help pages

## Concurrency / synchronization

- Much of the time you'll probably just use [database transactions](#SQL) to coordinate updates to data, but if you need something in-memory _start_ with something composable like [STM](http://hackage.haskell.org/package/stm)

- See the [note on concurrency](#note-on-concurrency) to learn more.

## CSV

- [cassava](https://hackage.haskell.org/package/cassava)
    - [HowIStart Haskell tutorial using Cassava](http://howistart.org/posts/haskell/1)

- [pipes-csv](http://hackage.haskell.org/package/pipes-csv) streaming, built on Cassava. See the [note on streaming libraries](#note-on-streaming).

## Bytes

- [bytestring](http://hackage.haskell.org/package/bytestring)

## Elasticsearch

- [Bloodhound](http://hackage.haskell.org/package/bloodhound) note you'll have to use Aeson version 0.10 or newer.

## File I/O

#### Text

- [Data.Text.IO](http://hackage.haskell.org/package/text/docs/Data-Text-IO.html)

#### Bytes

- [ByteString](http://hackage.haskell.org/package/bytestring/docs/Data-ByteString.html#g:26)'s core modules have the basic IO operations you'd need included.

## HTTP clients

#### I just got here and don't know what I'm doing

- [Wreq](https://hackage.haskell.org/package/wreq)
    - Tutorials and examples:
        - [Wreq tutorial](http://www.serpentine.com/wreq/)
        - [Keeping it easy while querying Github's API](http://bitemyapp.com/posts/2016-02-06-haskell-is-not-trivial-not-unfair.html)

#### Know what I'm doing and I'd like more fine-grained control over resources, such as streaming

Please [see the note on streaming](#note-on-streaming) before using any streaming libraries.

- [pipes-http](https://hackage.haskell.org/package/pipes-http)

- [http-conduit](https://hackage.haskell.org/package/http-conduit)


## JSON

- [aeson](http://hackage.haskell.org/package/aeson)
    - [Tutorial](https://artyom.me/aeson) without cargo culting

## lenses

- [lens](http://hackage.haskell.org/package/lens) you'll have to cargo cult early on and it's best if you're not a beginner, but there's nothing better if it's something you're going to use instead of framing the source code and mounting it over your fireplace.

## Logging

- [fast-logger](http://hackage.haskell.org/package/fast-logger) which uses [monad-logger](http://hackage.haskell.org/package/monad-logger) for the interface.
    - [Example](https://github.com/thoughtbot/carnival/blob/e78b8cdebecfdbdc19627523fe7b85b6ca61d426/Stripe.hs#L20) from [carnival](https://github.com/thoughtbot/carnival/blob/1e06131fec9f3e9a41a209cbe355439d17532c99/Application.hs#L17)

## Network I/O

[Network.Socket.Bytestring](https://hackage.haskell.org/package/network-2.6.2.1/docs/Network-Socket-ByteString.html) in the [network](https://hackage.haskell.org/package/network) library.

## NLP

- [chatter](http://hackage.haskell.org/package/chatter) a collection of simple Natural Language Processing algorithms.

## Parsing

- [attoparsec](http://hackage.haskell.org/package/attoparsec)

- [parsers](http://hackage.haskell.org/package/parsers) with [trifecta](http://hackage.haskell.org/package/trifecta) or [attoparsec](http://hackage.haskell.org/package/attoparsec) as the backend.
    - Covered in [Haskell book](http://haskellbook.com/) in the chapter on parsers.

## Regular expressions

- [regex-tdfa](http://hackage.haskell.org/package/regex-tdfa)

## SQL

#### I'm new and just want to throw a query over the wire!

##### PostgreSQL

- [postgresql-simple](hackage.haskell.org/package/postgresql-simple)

##### MySQL

- [mysql-simple](hackage.haskell.org/package/mysql-simple)

##### SQlite

- [sqlite-simple](http://hackage.haskell.org/package/sqlite-simple)

#### I'm not totally new to Haskell and willing to invest in something more type-safe!

- [Persistent](hackage.haskell.org/package/persistent) combined with [Esqueleto](hackage.haskell.org/package/esqueleto) as appropriate.
    - [Yesod book chapter on Persistent](http://www.yesodweb.com/book/persistent)
    - For Esqueleto? You're going to trip up a bit early on, especially with the join order. Try to accumulate example code you can work from.

## Statistics

- [statistics](http://hackage.haskell.org/package/statistics)

## Streaming

- [Pipes](http://hackage.haskell.org/package/pipes)

Take the [note on streaming](#note-on-streaming) under advisement. Don;t

## Testing

#### Unit/spec testing

- [HSpec](http://hspec.github.io/)
    - Covered in [Haskell Book](http://haskellbook.com)'s testing chapter.

#### Property testing

- [QuickCheck](http://hackage.haskell.org/package/QuickCheck)
    - [If you find yourself commonly checking typeclass laws](http://hackage.haskell.org/package/checkers)
    - Also covered in [Haskell Book](http://haskellbook.com)'s testing chapter and used throughout the chapters on monoid, semigroup, functor, applicative, and monad.

## Text / strings

- Use [text](http://hackage.haskell.org/package/text) if you're going to have a non-trivial amount of textual data in memory, but don't freak out about using the String type for small stuff. Sometimes String is faster anyway because of GHC's optimizations for lists.

## Time

- Funnily enough, [time](http://hackage.haskell.org/package/time) is the foundational library.

#### I need something faster than time or I use a lot of timestamps!

- [thyme](http://hackage.haskell.org/package/thyme)'s your bag.

## Utility libraries and conveniences

- [classy-prelude](http://hackage.haskell.org/package/classy-prelude) can save you some grief, particularly around mixing IO, String, ByteString, and Text. Best used with a [project template](https://github.com/commercialhaskell/stack-templates/blob/master/yesod-hello-world.hsfiles) that disables Prelude by default and gets an import module in place for you.

- [composition-extra](http://hackage.haskell.org/package/composition-extra) mostly for this [bad boy](http://hackage.haskell.org/package/composition-extra-2.0.0/docs/Data-Functor-Syntax.html#v:-60--36--36--62-). Double-fmap! I am easily amused.

## Web Frameworks

#### I just need to make a tiny API or I just started

- [Scotty](http://hackage.haskell.org/package/scotty)
    - [Examples in the Scotty git repository](https://github.com/scotty-web/scotty/tree/master/examples)
    - [Literate URL shortener](http://bitemyapp.com/posts/2014-11-22-literate-url-shortener.html)
  - [Compact URL shortener](http://bitemyapp.com/posts/2014-08-22-url-shortener-in-haskell.html)

#### I'm going to make a web application I have to maintain and I'm comfortable with a more opinionated framework

- [Yesod](http://www.yesodweb.com/) also better for APIs if you can handle something more advanced than Scotty.
    - [Yesod book](http://www.yesodweb.com/book)
    - [Carnival](https://github.com/thoughtbot/carnival) open source comments app.
    - [Snowdrift.coop](https://github.com/snowdriftcoop/snowdrift) a non-profit, cooperative platform for funding public goods, specifically Free/Libre/Open (FLO) works.

## XML

- [taggy](https://hackage.haskell.org/package/taggy) and [taggy-lens](https://hackage.haskell.org/package/taggy-lens).

- [xml-conduit](https://hackage.haskell.org/package/xml-conduit) the usual caveats around streaming apply.

## Note on concurrency

For learning how to write parallel and concurrent programs in Haskell, [Marlow's book](http://chimera.labs.oreilly.com/books/1230000000929) is peerless.

## Note on streaming

Pipes is generally easier to use than Conduit and can be quite pleasant. However, if you're new to Haskell you should avoid using streaming libraries unless necessary to control memory usage. Mostly because you'll spin your wheels on which operator to use and the type errors won't help much. YMMV.
