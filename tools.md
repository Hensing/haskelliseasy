---
title: Tools
---

# Haskell is easy - recommended tools

## Profiling / Performance

- [profiteur](https://hackage.haskell.org/package/profiteur) gives a treemap visualization to [GHC profiles](https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/profiling.html)

- [ThreadScope](http://hackage.haskell.org/package/threadscope) is a graphical tool for visualizing concurrency and parallelism of running programs

- [ekg](http://hackage.haskell.org/package/ekg) is a monitoring framework which has built-in support for GHC stats and allows retrieving details via JSON
